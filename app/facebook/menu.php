<style type="text/css">
  .sidebar-wrapper>.nav [data-toggle=collapse]~div>ul>li>a .sidebar-normal {
    font-size: 10px;
  }
</style>
<html>

<body>


<div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="../assets/img/iconcool.png">
          </div>
        </a>
        <a href="../index.php" class="simple-text logo-normal">
          SSG
          <!-- <div class="logo-image-big">
            <img src="../../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
       <div class="sidebar-wrapper">
           <ul class="nav">
          <li>
            <a href="../index.php">
              <i class="nc-icon nc-bank"></i>
              <p>Panel</p>
            </a>
          </li>
     
          <li id="r">
            <a data-toggle="collapse" href="#reporte">
              <i class="nc-icon nc-book-bookmark"></i>
              <p>
                Reportes Google
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse"  id="reporte">
              <ul class="nav">
                <!-- <li id="rmp15">-->
                <!--  <a href="./reporteproductomedio15.php">-->
                <!--    <span class="sidebar-mini-icon">MP</span>-->
                <!--    <span class="sidebar-normal">Medios - Producto 15 dias </span>-->
                <!--  </a>-->
                <!--</li>-->
               
                <!--<li id="fdf">-->
                <!--  <a href="./reportefacebookform.php">-->
                <!--    <span class="sidebar-mini-icon">FAR</span>-->
                <!--    <span class="sidebar-normal"> Facebook - Avisos - Resultados </span>-->
                <!--  </a>-->
                <!--</li>-->
                <!--<li id="gga">-->
                <!--  <a href="./reportegooglegak.php">-->
                <!--    <span class="sidebar-mini-icon">GAR</span>-->
                <!--    <span class="sidebar-normal"> Google Anucios - Resultados </span>-->
                <!--  </a>-->
                <!--</li>-->
                <!-- <li id="rm">-->
                <!--  <a href="./emailsgeneral.php">-->
                <!--    <span class="sidebar-mini-icon">RGM</span>-->
                <!--    <span class="sidebar-normal"> Resultados Generales Emails</span>-->
                <!--  </a>-->
                <!--</li>-->
                <!--<li id="rme">-->
                <!--  <a href="./emailsesp.php">-->
                <!--    <span class="sidebar-mini-icon">REE</span>-->
                <!--    <span class="sidebar-normal"> Resultados Específico Email</span>-->
                <!--  </a>-->
                <!--</li>-->
                <!-- <li id="fd">-->
                <!--  <a href="./reportefacebook.php">-->
                <!--    <span class="sidebar-mini-icon">FDD</span>-->
                <!--    <span class="sidebar-normal"> Facebook Desempeño Demográfico </span>-->
                <!--  </a>-->
                <!--</li>-->
                <li id="gck">
                  <a href="./reportegoogle.php">
                    <span class="sidebar-mini-icon">GDK</span>
                    <span class="sidebar-normal"> Google Desempeño de Keywords </span>
                  </a>
                </li>
                <!-- <li id="gdf">-->
                <!--  <a href="./formevo.php">-->
                <!--    <span class="sidebar-mini-icon">GDF</span>-->
                <!--    <span class="sidebar-normal"> Graficos Desempeños Form </span>-->
                <!--  </a>-->
                <!--</li>-->
                <!-- <li id="dck">-->
                <!--  <a href="./dashboard.php">-->
                <!--    <span class="sidebar-mini-icon">DCK</span>-->
                <!--    <span class="sidebar-normal"> Dashboard  </span>-->
                <!--  </a>-->
                <!--</li>-->
              </ul>
            </div>
          </li>
           <!--<li id="s">-->
           <!-- <a data-toggle="collapse" href="#ss">-->
           <!--   <i class="nc-icon nc-layout-11"></i>-->
           <!--   <p>-->
           <!--     SSG-->
           <!--     <b class="caret"></b>-->
           <!--   </p>-->
           <!-- </a>-->
            
           
          <!-- <div class="collapse " id="ss">-->
          <!--    <ul class="nav">-->
          <!--      <li id="ssg">-->
          <!--        <a href="https://ssgenius.com/app/facebook/">-->
          <!--          <span class="sidebar-mini-icon">SSG</span>-->
          <!--          <span class="sidebar-normal">Social Stats Genius</span>-->
          <!--        </a>-->
          <!--      </li>-->
          <!--    </ul>-->
          <!--  </div> -->
          <!--</li>-->

          <!-- <li id="f">-->
          <!--  <a data-toggle="collapse" href="#facebook">-->
          <!--    <i class="nc-icon nc-chart-bar-32"></i>-->
          <!--    <p>-->
          <!--      FACEBOOK-->
          <!--      <b class="caret"></b>-->
          <!--    </p>-->
          <!--  </a>-->
            
           
          <!-- <div class="collapse " id="facebook">-->
          <!--    <ul class="nav">-->
          <!--      <li id="grf">-->
          <!--        <a href="./graficosfacebook.php">-->
          <!--          <span class="sidebar-mini-icon">GRF</span>-->
          <!--          <span class="sidebar-normal">Gráficos Facebook</span>-->
          <!--        </a>-->
          <!--      </li>-->
          <!--      <li>-->
          <!--        <a href="reporteinformes.php">-->
          <!--          <span class="sidebar-mini-icon">INF</span>-->
          <!--          <span class="sidebar-normal"> Informes CRUD </span>-->
          <!--        </a>-->
          <!--      </li>-->
          <!--      <li>-->
          <!--        <a href="reportekpi.php">-->
          <!--          <span class="sidebar-mini-icon">KPI</span>-->
          <!--          <span class="sidebar-normal"> Informe KPIs (atajo) </span>-->
          <!--        </a>-->
          <!--      </li>-->
          <!--      <li>-->
          <!--        <a href="definirconsultoria.php">-->
          <!--          <span class="sidebar-mini-icon">IC</span>-->
          <!--          <span class="sidebar-normal"> Definir Consultorias </span>-->
          <!--        </a>-->
          <!--      </li>-->
          <!--      <li>-->
          <!--        <a href="reporteconsultoria.php">-->
          <!--          <span class="sidebar-mini-icon">IC</span>-->
          <!--          <span class="sidebar-normal"> Informes Consultorias </span>-->
          <!--        </a>-->
          <!--      </li>-->
          <!--    </ul>-->
          <!--  </div> -->
          <!--</li>-->
          <li>
            <a href="../logout.php">
              <!--<i class="nc-icon nc-button-power"></i>-->
              <!--<p>Salir</p>-->
            </a>
          </li>
        </ul>
        <div class="user">
            <hr>
            
                <div class="photo">
                  <!--<img src="../assets/img/default-avatar.png" />-->
                </div>
                <div class="info">
                  <!--<a data-toggle="collapse" href="#collapseExample" class="collapsed">-->
                    <span>
                      <?php echo $nombre; ?>
                      
                    </span>
                  </a>
                  <div class="clearfix"></div>
                  <div class="collapse" id="collapseExample">
                    
                  </div>
                </div>
        </div>
      </div>



    </div>
</body>
</html>
